require 'fileutils'

HOME = Dir.home

module Dotfiles
  PATH = File.expand_path File.dirname __FILE__

  class Helper
    BACKUP_DIR_PATH = File.join PATH, ".backups"

    attr_reader :module_name

    def initialize module_name
      @module_name = module_name
    end

    def module_path
      File.join PATH, @module_name
    end

    def backup_dir_path
      File.join BACKUP_DIR_PATH, @module_name
    end

    # Backs up given elements.
    # Elements are saved into ./backups/<module>
    def back_up(*elements)
      FileUtils.mkdir_p backup_dir_path

      elements.each do |element|
        if File.exists? element
          if File.symlink? element
            # One has previously bind its file to another system
            FileUtils.rm element
          else
            # Save the user file
            FileUtils.mv element, backup_dir_path
          end
        end
      end
    end

    def restore file, destination_path
      backup_file = File.join backup_dir_path, file
      if File.exists? backup_file
        desination_file = File.join destination_path, file
        FileUtils.rm desination_file if File.exists? desination_file
        FileUtils.mv backup_file, destination_path
      else
        $stderr.puts "File #{file} is not in #{@module_name} backup"
      end
    end

  end

  def self.generate_config(directory)
    # stuff to do
  end
end

namespace :bash do
  helper = Dotfiles::Helper.new "bash"

  desc "Install bashrc configuration"
  task :install do
    puts "Installing bas configuration"

    configuration_file = File.join HOME, '.bashrc'
    helper.back_up configuration_file

    File.symlink File.join(helper.module_path, '.bashrc'), configuration_file

    puts <<-NOTE
The bash configuration has been installed.
You can add your local configuration by creating additional scripts in #{File.join Dotfiles::PATH, helper.module_name, 'extended'}.
    NOTE
  end

  desc "Clear bash installation"
  task :uninstall do
    puts "Removing bash installation"

    helper.restore '.bashrc', HOME

    puts <<-NOTE
The bash installation has been removed and the previous configuration restore.
    NOTE
  end
end

namespace :awesome do
  configuration_file = ".config/awesome/rc.lua"
  module_name = "awesome"

  desc "Install awesome configuration in your environment"
  task :install do
    puts "Installing awesome configuration ..."

    # save previous configuration
    # Dotfiles::back_up(configuration_file, module_name)

    # # generate user confuguration (*.example)
    # Dotfiles::generate_config(module_name + '/defined')

    # # link new configuration
    # ln(module_name + "/rc.lua", configuration_file)

    puts <<-HELP
User defined configuration can be setted into #{}/extended directory.
All files present will be loaded before any configuration is applied.
    HELP
  end

  desc "Removes awesome configuration from your environment"
  task :uninstall do
    # Remove rc.lua symbolic link

    # Restore backed up files
    Dotfiles::restore(module_name, configuration_file)
  end
end
