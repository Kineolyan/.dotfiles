-- Configure bindings to use i3lock
-- ModKey + F12 -> screen lock

local awful = require("awful")

globalkeys = awful.util.table.join(globalkeys,
  awful.key({ modkey,           }, "F12",   function () awful.util.spawn('i3lock -c 000000') end)
)
