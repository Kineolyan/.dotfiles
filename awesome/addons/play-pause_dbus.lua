-- Configure bindings to use control spotify with DBus events

local awful = require("awful")

local cmds = {
  play = 'dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.PlayPause',
  stop = 'dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Stop',
  next = 'dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Next',
  previous = 'dbus-send --print-reply --dest=org.mpris.MediaPlayer2.spotify /org/mpris/MediaPlayer2 org.mpris.MediaPlayer2.Player.Previous'
}

globalkeys = awful.util.table.join(globalkeys,
  awful.key({ }, "XF86AudioPlay",   function () awful.util.spawn(cmds.play) end),
  awful.key({ }, "XF86AudioNext",   function () awful.util.spawn(cmds.next) end),
  awful.key({ }, "XF86AudioPrev",   function () awful.util.spawn(cmds.previous) end)
)
