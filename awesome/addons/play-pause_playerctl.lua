-- Configure bindings to use control spotify with DBus events

local awful = require("awful")

local cmds = {
  play = 'playerctl play-pause',
  stop = 'playerctl stop',
  next = 'playerctl next',
  previous = 'playerctl previous'
}

globalkeys = awful.util.table.join(globalkeys,
  awful.key({ }, "XF86AudioPlay",   function () awful.util.spawn(cmds.play) end),
  awful.key({ }, "XF86AudioNext",   function () awful.util.spawn(cmds.next) end),
  awful.key({ }, "XF86AudioPrev",   function () awful.util.spawn(cmds.previous) end)
)
