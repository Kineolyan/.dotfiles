local wibox = require("wibox")

battery = {}
battery.widget = wibox.widget.textbox()    
battery.widget:set_text("| Battery |")    
battery.timer = timer({ timeout = 5 })    
battery.timer:connect_signal("timeout",    
  function()    
    fh = assert(io.popen("acpi | cut -d, -f 2,3 -", "r"))    
    battery.widget:set_text("Bt " .. fh:read("*l"))    
    fh:close()    
  end    
)
battery.start = function(self)
  self.timer:start()
end

-- To start the widget, do:
-- battery:start()

return battery
