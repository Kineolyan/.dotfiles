local awful = require("awful")
local wibox = require("wibox")
local beautiful = require("beautiful")

-- Load Debian menu entries
-- Remove debian specification
-- require("debian.menu")

local configuration = require("configuration")
configuration:load(beautiful)

-- Create a laucher widget and a main menu
myawesomemenu = {
	{ "manual", configuration.settings.terminal .. " -e man awesome" },
	{ "edit config", configuration.settings.edit_cmd .. " " .. awesome.conffile },
	{ "restart", awesome.restart },
	{ "quit", awesome.quit }
}

function systemctl(cmd)
	awful.util.spawn("sudo systemctl " .. cmd)
end

power_menu = {
	{ "shutdown", function() systemctl("poweroff") end },
	{ "reboot", function() systemctl("reboot") end }
}

mymainmenu = awful.menu({
	items = {
		{ "awesome", myawesomemenu, beautiful.awesome_icon },
--  { "Debian", debian.menu.Debian_menu.Debian },
		{ "Power", power_menu, beautiful.power_icon },
    { "open terminal", configuration.settings.terminal },
    { "open Settings", configuration.settings.settings }
  }
})

mylauncher = awful.widget.launcher({ image = beautiful.awesome_icon,
                                     menu = mymainmenu })

-- Create a textclock widget
mytextclock = awful.widget.textclock()

-- Keyboard map indicator and switcher
-- mykeyboardlayout = awful.widget.keyboardlayout()

-- Create a systray
mysystray = wibox.widget.systray()

-- Widget to display tags
mytaglist = {}
mytaglist.buttons = awful.util.table.join(
  awful.button({ }, 1, awful.tag.viewonly),
  awful.button({ modkey }, 1, awful.client.movetotag),
  awful.button({ }, 3, awful.tag.viewtoggle),
  awful.button({ modkey }, 3, awful.client.toggletag),
  awful.button({ }, 4, function(t) awful.tag.viewnext(awful.tag.getscreen(t)) end),
  awful.button({ }, 5, function(t) awful.tag.viewprev(awful.tag.getscreen(t)) end)
)

-- Widget to display tasks
mytasklist = {}
mytasklist.buttons = awful.util.table.join(
  awful.button({ }, 1, function (c)
    if c == client.focus then
      c.minimized = true
    else
      if not c:isvisible() then
        awful.tag.viewonly(c:tags()[1])
      end
      -- This will also un-minimize
      -- the client, if needed
      client.focus = c
      c:raise()
    end
  end),
  awful.button({ }, 3, function ()
    if instance then
      instance:hide()
      instance = nil
    else
      instance = awful.menu.clients({ width=250 })
    end
  end),
  awful.button({ }, 4, function ()
    awful.client.focus.byidx(1)
    if client.focus then client.focus:raise() end
  end),
  awful.button({ }, 5, function ()
    awful.client.focus.byidx(-1)
    if client.focus then client.focus:raise() end
  end)
)
