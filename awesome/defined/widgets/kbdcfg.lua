local wibox = require("wibox")
local awful = require("awful")

kbdcfg = {}
kbdcfg.cmd = "setxkbmap"
kbdcfg.layout = { { "us altgr-intl", "us" }, { "fr", "fr" }, { "us dvp", "dvp" } }
kbdcfg.current = 0  -- us is our default layout
kbdcfg.get_layout = function() return kbdcfg.layout[kbdcfg.current + 1] end
kbdcfg.get_text = function () return "[ " .. kbdcfg.get_layout()[2] .. " ]" end
kbdcfg.config_layout = function() os.execute( kbdcfg.cmd .. " " .. kbdcfg.get_layout()[1] ) end
kbdcfg.widget = wibox.widget.textbox()
kbdcfg.widget:set_text(kbdcfg.get_text())
kbdcfg.switch = function (inc)
  local size = #(kbdcfg.layout)
  kbdcfg.current = (kbdcfg.current + inc + size) % size
  kbdcfg.widget:set_text(kbdcfg.get_text())
  kbdcfg.config_layout()
end

-- Mouse bindings
kbdcfg.widget:buttons(awful.util.table.join(
  awful.button({ }, 1, function () kbdcfg.switch(1) end),
  awful.button({ }, 3, function () kbdcfg.switch(-1) end)
))

return kbdcfg
