local wibox = require("wibox")

-- Separator
separator = {}
separator.widget = wibox.widget.textbox()
separator.widget:set_text(" :: ")

return separator
