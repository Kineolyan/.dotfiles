local wibox = require("wibox")
local awful = require("awful")
local naughty = require("naughty")

-- Volume widget
volumecfg = {}
volumecfg.cardid  = 0
volumecfg.channel = "Master"
volumecfg.widget = wibox.widget.textbox()

-- volumecfg_t = awful.tooltip({ objects = { volumecfg.widget },})
-- volumecfg_t:set_text("Volume")

volumecfg.getstatus = function (command)
       local fd = io.popen("amixer -c " .. volumecfg.cardid .. " " .. command)
       local status = fd:read("*all")
       fd:close()
       return status
end
volumecfg.mixercommand = function (command)
       local status = volumecfg.getstatus(command)

       local volume = string.match(status, "(%d?%d?%d)%%")
       volume = string.format("% 3d", volume)
       status = string.match(status, "%[(o[^%]]*)%]")
       if string.find(status, "on", 1, true) then
               volume = volume .. "%"
       else
               volume = volume .. "M"
       end
       volumecfg.widget:set_text("Vol: " .. volume)
end
volumecfg.update = function ()
       volumecfg.mixercommand("sget " .. volumecfg.channel)
end
volumecfg.up = function ()
       volumecfg.mixercommand("sset " .. volumecfg.channel .. " 1%+")
end
volumecfg.down = function ()
       volumecfg.mixercommand("sset " .. volumecfg.channel .. " 1%-")
end
-- volumecfg.toggle = function ()
--        volumecfg.mixercommand("sset " .. volumecfg.channel .. " toggle")
-- end
volumecfg.show = function ()
       return volumecfg.getstatus("sget " .. volumecfg.channel)
end
volumecfg.widget:buttons(awful.util.table.join(
       awful.button({ }, 4, function () volumecfg.up() end),
       awful.button({ }, 5, function () volumecfg.down() end),
--        awful.button({ }, 1, function () volumecfg.toggle() end),
       awful.button({ }, 3, function () naughty.notify({
           text = string.format('<span font_desc="%s">%s</span>', "monospace", volumecfg.show()),
           timeout = 0,
           hover_timeout = 0.5,
           screen = mouse.screen
         })
       end)
))
volumecfg.update()

return volumecfg
