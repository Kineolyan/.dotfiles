package.path = os.getenv("HOME") .. "/.dotfiles/awesome/?.lua;" .. package.path

-- Standard awesome library
local awful = require("awful")
awful.rules = require("awful.rules")
require("awful.autofocus")

-- Widgets

-- Notification library
local naughty = require("naughty")

-- {{{ Error handling
-- Check if awesome encountered an error during startup and fell back to
-- another config (This code will only ever execute for the fallback config)
if awesome.startup_errors then
  naughty.notify({
    preset = naughty.config.presets.critical,
    title = "Oops, there were errors during startup!",
    text = awesome.startup_errors
  })
end

-- Handle runtime errors after startup
do
  local in_error = false
  awesome.connect_signal("debug::error", function (err)
    -- Make sure we don't go into an endless error loop
    if in_error then return end
    in_error = true

    naughty.notify({
      preset = naughty.config.presets.critical,
      title = "Oops, an error happened!",
      text = err
    })
    in_error = false
  end)
end
-- }}}

-- VARIABLES
local configuration = require("configuration")
local config_paths = configuration.get_paths()

-- Default modkey (Mod4 = WinKey)
modkey = configuration.modkey

-- TAGS (screens) && LAYOUTS
local tags = require("defined.tags")
for s = 1, screen.count() do
  tags[s] = awful.tag(tags.names, s, tags.layouts)
end

-- Default wibox stuff
dofile(config_paths.defined .. '/widgets.lua')

-- Key && mouse bindings
dofile(config_paths.settings .. "/buttons.lua")
dofile(config_paths.settings .. "/keys.lua")
dofile(config_paths.settings .. "/clientbuttons.lua")
dofile(config_paths.settings .. "/clientkeys.lua")

-- Rules
dofile(config_paths.settings .. "/rules.lua")

-- Signals
dofile(config_paths.settings .. "/signals.lua")

-- Background generation
dofile(config_paths.settings .. "/background.lua")

-- Loadings add-ons
for index, addon_file in ipairs(configuration.addons) do
  dofile(config_paths.addons .. "/" .. addon_file)
end

-- Loadings extensions files
for index, extension_file in ipairs(configuration.extensions) do
  dofile(config_paths.extended .. "/" .. extension_file)
end

-- Set keys
root.keys(globalkeys)
root.buttons(globalbuttons)
awful.rules.rules = globalrules

for signal, action in pairs(globalsignals) do
  client.connect_signal(signal, action)
end
