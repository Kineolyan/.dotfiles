-- | Background configuration | --
-- This includes wallpaper and conky

local gears = require("gears")
local beautiful = require("beautiful")

local configuration = require("configuration")
configuration:load(beautiful)

-- | Wallpaper | --
-- scan directory, and optionally filter outputs
function scandir(directory, filter)
    local i, t, popen = 0, {}, io.popen
    if not filter then
        filter = function(s) return true end
    end

    -- List of visible files in the directory
    for filename in popen('ls "'..directory..'"'):lines() do
        if filter(filename) then
            i = i + 1
            t[i] = filename
        end
    end
    return t
end

local wallpaper_to_display = nil
if beautiful.wallpaper then
	wallpaper_to_display = beautiful.wallpaper
elseif beautiful.wallpaper_dir then
	wallpapers = scandir(beautiful.wallpaper_dir)
  -- get next random index
  wp_index = math.random( 1, #wallpapers)
  wallpaper_to_display = beautiful.wallpaper_dir .. "/" .. wallpapers[wp_index]
end

if wallpaper_to_display then
  for s = 1, screen.count() do
    gears.wallpaper.maximized(wallpaper_to_display, s)
  end
end

-- | Conky | --
-- To come ...
