local awful = require("awful")
local configuration = require("configuration")

-- {{{ Key bindings

function nextApp ()
    awful.client.focus.byidx( 1)
    if client.focus then client.focus:raise() end
end
function previousApp ()
    awful.client.focus.byidx(-1)
    if client.focus then client.focus:raise() end
end
function nextTag()
  awful.tag.viewnext()
end
function previousTag()
  awful.tag.viewprev()
end

function nextScreen()
  awful.screen.focus_relative(-1)
end
function previousScreen()
  awful.screen.focus_relative( 1)
end

function toggleMinimized(minimized)
  local tag = awful.tag.selected()
  for i = 1, #tag:clients() do
    tag:clients()[i].minimized = minimized
  end
end

globalkeys = awful.util.table.join(
    awful.key({ modkey,           }, "Escape", awful.tag.history.restore),

    awful.key({ modkey,           }, "k", nextApp),
    awful.key({ modkey,           }, "j", previousApp),
    awful.key({ modkey,           }, "Right", nextApp),
    awful.key({ modkey,           }, "Left", previousApp),
    awful.key({ modkey, "Shift"   }, "j",      previousTag ),
    awful.key({ modkey, "Shift"   }, "k",      nextTag     ),
    awful.key({ modkey, "Shift"   }, "Left",   previousTag ),
    awful.key({ modkey, "Shift"   }, "Right",  nextTag     ),
    awful.key({ modkey, "Control" }, "j",     previousScreen ),
    awful.key({ modkey, "Control" }, "k",     nextScreen     ),
    awful.key({ modkey, "Control" }, "Left",  previousScreen ),
    awful.key({ modkey, "Control" }, "Right", nextScreen     ),
    awful.key({ modkey,           }, "w", function () mymainmenu:show({keygrabber=true}) end),

    -- Layout manipulation
    awful.key({ modkey, "Shift"   }, "u", function () awful.client.swap.byidx(  1)    end),
    awful.key({ modkey, "Shift"   }, "i", function () awful.client.swap.byidx( -1)    end),
    awful.key({ modkey,           }, "u", awful.client.urgent.jumpto),
    awful.key({ modkey,           }, "Tab",
        function ()
            awful.client.focus.history.previous()
            if client.focus then
                client.focus:raise()
            end
        end),

    -- Standard program
    awful.key({ modkey,           }, "Return", function () awful.util.spawn(configuration.settings.terminal) end),
    awful.key({ modkey, "Control" }, "r", awesome.restart),
    awful.key({ modkey, "Shift"   }, "q", awesome.quit),

    awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)    end),
    awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)    end),
    awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1)      end),
    awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1)      end),
    awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1)         end),
    awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1)         end),
    awful.key({ modkey,           }, "space", function () awful.layout.inc(awful.layout.layouts, 1) end),
    awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(awful.layout.layouts,-1) end),

    awful.key({ modkey, "Control" }, "n", awful.client.restore),
    awful.key({ modkey            }, "d",     function() toggleMinimized(true) end),
    awful.key({ modkey, "Shift"   }, "d",     function() toggleMinimized(false) end),

    -- Prompt
    awful.key({ modkey            }, "r",     function () mypromptbox[mouse.screen]:run() end),

    awful.key({ modkey            }, "x",     function ()
      awful.prompt.run({ prompt = "Run Lua code: " },
      mypromptbox[mouse.screen].widget,
      awful.util.eval, nil,
      awful.util.getdir("cache") .. "/history_eval")
    end),

    awful.key({     }, "XF86AudioRaiseVolume", function() awful.util.spawn("amixer set Master 1%+", false) end),
    awful.key({     }, "XF86AudioLowerVolume", function() awful.util.spawn("amixer set Master 1%-", false) end)
)

if configuration.settings.fileManager then
  globalkeys = awful.util.table.join(globalkeys,
    awful.key({ modkey,           }, "e", function () awful.util.spawn(configuration.settings.fileManager) end)
  )
end

-- Compute the maximum number of digit we need, limited to 9
keynumber = 0
for s = 1, screen.count() do
   keynumber = math.min(9, math.max(#tags[s], keynumber));
end

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it works on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, keynumber do
    globalkeys = awful.util.table.join(globalkeys,
        awful.key({ modkey }, "#" .. i + 9,
                  function ()
                        local screen = mouse.screen
                        if tags[screen][i] then
                            awful.tag.viewonly(tags[screen][i])
                        end
                  end),
        awful.key({ modkey, "Control" }, "#" .. i + 9,
                  function ()
                      local screen = mouse.screen
                      if tags[screen][i] then
                          awful.tag.viewtoggle(tags[screen][i])
                      end
                  end),
        awful.key({ modkey, "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.movetotag(tags[client.focus.screen][i])
                      end
                  end),
        awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
                  function ()
                      if client.focus and tags[client.focus.screen][i] then
                          awful.client.toggletag(tags[client.focus.screen][i])
                      end
                  end))
end
-- Key bindings }}}
