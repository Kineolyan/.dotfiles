local beautiful = require("beautiful")

local configuration = require("configuration")
configuration:load(beautiful)

-- {{{ Rules
globalrules = {
    -- All clients will match this rule.
    { rule = { },
      properties = { border_width = beautiful.border_width,
                     border_color = beautiful.border_normal,
                     focus = true,
                     keys = clientkeys,
                     buttons = clientbuttons } },
    { rule = { class = "MPlayer" },
      properties = { floating = true } },
    { rule = { class = "pinentry" },
      properties = { floating = true } },
    { rule = { class = "gimp" },
      properties = { floating = true } },
    { rule_any = { class = {"Gitk", "Bcompare", "Git-cola", "GitKraken", "Meld"} },
      properties = { maximized = true, floating = false } },
    {
      rule_any = { class = { "Pidgin", "Skype", "HipChat" } },
      properties = { tag = tags[1][9] }
    }, {
      rule_any = { class = {"jetbrains-idea-ce", "jetbrains-webstorm"} },
      except_any = { type = { "dialog" } },
      properties = { tag = tags[1][3] }
    }, {
      rule = { class = "Eclipse", type = "normal" },
      properties = { tag = tags[1][3], maximized = true }
    }
}
-- }}}
