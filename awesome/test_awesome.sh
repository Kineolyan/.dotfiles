#!/bin/bash
# Run Awesome in a nested server for tests
#
# Requirements: (Debian)
#
#  apt-get install xserver-xephyr
#  apt-get install -t unstable awesome
#
# Based on original script by dante4d <dante4d@gmail.com>
# See: http://awesome.naquadah.org/wiki/index.php?title=Using_Xephyr
#
# URL: http://hellekin.cepheide.org/awesome/awesome_test
#
# Copyright (c) 2009 Hellekin O. Wolf <hellekin@cepheide.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

function usage()
{
  cat <<USAGE
awesome_test start|stop|restart|run

  start    Start nested Awesome in Xephyr
  stop     Stop Xephyr
  restart  Reload nested Awesome configuration
  run      Run command in nested Awesome

USAGE
  exit 0
}

# WARNING: the following two functions expect that you only run one instance
# of Xephyr and the last launched Awesome runs in it

function xephyr_pid()

{
  /bin/pidof Xephyr | cut -d\  -f1
}

[ $# -lt 1 ] && usage

readonly args=$(getopt -s bash -o "c:,x:" -- "$@")
if [ $? -ne 0 ]
then
  echo -e "\e[31mErrors on parameters\e[0m"
  exit 1
fi

RC_LUA=$HOME/.config/awesome/rc.lua
AWESOME=`which awesome`
XEPHYR=`which Xephyr`

function start() {
  $XEPHYR -ac -br -noreset -screen 800x600 :1 &
  sleep 1
  DISPLAY=:1.0 $AWESOME -c $RC_LUA &
  awesome_pid=$!

  sleep 1
  echo Awesome ready for tests. PID is $awesome_pid
}

function stop() {
  echo -n "Stopping Nested Awesome... "
  if [ -z $(xephyr_pid) ]; then
    echo "Not running: not stopped :)"
    exit 0
  else
    kill $awesome_pid $(xephyr_pid)
    echo "Done."
  fi
}

function restart() {
  echo -n "Restarting nested Awesome... "
  # kill -s SIGHUP $awesome_pid
}

function run() {
  DISPLAY=:1.0 "$@" &
}

# Fetch options
eval set -- "$args"
while true
do
  option="$1"
  shift 1
  case "$option" in
    -c)
      RC_LUA="$1"
      shift 1 ;;
    -x)
      AWESOME="$1"
      shift 1 ;;
    --) break ;;
    *) echo "Invalid option $option"
  esac
done

test -f $RC_LUA || { echo "Invalid configuration file $RC_LUA"; exit 1; }
test -x $AWESOME || { echo "Awesome executable not found. Please install Awesome"; exit 1; }
test -x $XEPHYR || { echo "Xephyr executable not found. Please install Xephyr"; exit 1; }

case "$1" in
  start) start ;;
  stop) stop ;;
  restart) stop && start ;;
  run)
    shift
    run $@
    ;;
  *) usage ;;
esac
