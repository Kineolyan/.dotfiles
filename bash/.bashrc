# ~/.bashrc: executed by bash(1) for non-login shells.
# see /usr/share/doc/bash/examples/startup-files (in the package bash-doc)
# for examples

# If not running interactively, don't do anything
case $- in
    *i*) ;;
      *) return;;
esac

DOT_PATH=$HOME/.dotfiles
# loader of extensions
load() {
  [ -f $DOT_PATH/bash/$1 ] && source $DOT_PATH/bash/$1
}

case "$OSTYPE" in
darwin*) ;; # Mac OS, do nothing
*) ;; # By default, do nothing source $HOME/.bash_profile ;;
esac

load configuration.sh
load environment.sh
load git.sh
load functions.sh
load aliases.sh
load completion.sh
load prompt.sh

if [ -d $DOT_PATH/bash/extended ]
then
  for extension in $(ls $DOT_PATH/bash/extended/*)
  do
    source $extension
  done
fi

# Be ready to reload anything if needed
kin_reload_bash() {
  source $HOME/.bashrc
}
