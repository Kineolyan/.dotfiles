# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    #alias dir='dir --color=auto'
    #alias vdir='vdir --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias rgrep='rgrep --color=auto'
    alias Rgrep="grep -R --color=auto"
fi

# cd aliases
alias ..='cd ../'
alias ...='cd ../../'
alias ....='cd ../../../'
alias ..4='cd ../../../../'
alias ..5='cd ../../../../../'

# some more ls aliases
alias ll='ls -alFh'
alias la='ls -A'
alias l='ls -CF'
alias lla='ls -alh'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'

# Basic commands
alias less="less -R"
alias rm="rm -I --preserve-root"

# basic binaries
alias rvim='vim -R'

# Git
alias gka='gitk --all -n 5000'
alias gf='git fetch -p'

# Mount
alias cmount='mount | column -t'

# Network
# Stop after sending count ECHO_REQUEST packets #
alias ping='ping -c 5'
# Do not wait interval 1 second, go fast #
alias fastping='ping -c 100 -s.2'
alias ports='netstat -tulanp'

# apt-get
alias apt-update='sudo apt-get update && sudo apt-get upgrade && sudo apt-get clean'
alias apt-update-y='sudo apt-get update && sudo apt-get upgrade --yes && sudo apt-get clean'
alias apt-dist-upgrade='sudo apt-get dist-upgrade'

# Disk usage
alias df='df -H'
alias du='du -ch'
