for path in "$HOME/bin" "$HOME/node_modules/.bin" "$HOME/.rbenv/bin"
do
	[ -d "$path" ] && export PATH="$path:$PATH"
done

if [ -d "$HOME/.rbenv/bin" ]
then # rbenv installed, initialize it
	 eval "$(rbenv init -)"
fi

export NVM_DIR="$HOME/.nvm"
if [ -s "$NVM_DIR/nvm.sh" ]
then # initialive nvm
	. "$NVM_DIR/nvm.sh"
fi
