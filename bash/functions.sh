# Create and go into a directory
mcd() {
  mkdir -pv $1;
  cd $1;
}

get-children() {
	local pid=$1
	[ -z "$pid" ] && pid=$$

	echo -n $pid
	for p in $(pgrep -P $pid)
	do
		echo -n ' '
		get-children $p
	done
}

abs_path() {
	for entry in $*
	do
		echo $PWD/${entry##/}
	done
}

ducks() {
	# From http://www.cyberciti.biz/faq/how-do-i-find-the-largest-filesdirectories-on-a-linuxunixbsd-filesystem/
	du -ckhs $* | sort -rh | head -n 11
}

which-ps () {
	local pattern=$1
	if [ -z "$pattern" ]
	then
		ps -ef
	else
		ps -ef | grep -E "$pattern"
	fi
}
