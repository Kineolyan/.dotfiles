function _install_git_prompt() {
  # Is git completion already installed
  type -t __git_ps1 &> /dev/null && return 0 # yes

  # Look for git prompt for bash completion
  [ -e /etc/bash_completion.d/git-prompt ] && return 0 # already there, it will load

  # Else look for a script in git-core
  GIT_COMPLETION=/usr/share/git-core/contrib/completion/git-prompt.sh
  [ -e "$GIT_COMPLETION" ] && source "$GIT_COMPLETION"
}

_install_git_prompt

function _git_prompt() {
  local git_status="`LANG=C git status -unormal 2>&1`"
  if ! [[ "$git_status" =~ Not\ a\ git\ repo ]]; then
    if [[ "$git_status" =~ nothing\ to\ commit ]]; then
      local ansi=32
    elif [[ "$git_status" =~ nothing\ added\ to\ commit\ but\ untracked\ files\ present ]]; then
      local ansi=34
    else
      local ansi=33
    fi

    local branch=$(__git_ps1)
    echo -n "\[\e[0;33;${ansi}m\]${branch# }\[\e[0m\]"
  fi
}

function _git_branch {
	if ! git rev-parse --git-dir > /dev/null 2>&1; then
		return 0
	fi

	git_branch=$(LANG=C git branch 2>/dev/null | sed -n '/^*/s/^\* //p')
	# set branch to _none_ if no branch
	[[ $git_branch == *no*branch* ]] && git_branch=''
	# if git diff --quiet 2>/dev/null >&2; then
	# 	git_color="${c_git_clean}"
  	# else
	# 	git_color=${c_git_cleanit_dirty}
	# fi

	if [ -n $git_branch ]; then
		echo "[$git_branch]"
	else
		echo ""
	fi
}
