#!/bin/sh
readonly DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )

# Not sure why this is needed
sleep 2

readonly conkies='conkyrc0'
for conkyFile in $conkies
do
  conky -c "$DIR/$conkyFile" &> /dev/null &
done
# conky -c ~/.conky/conkyrc2 &
# conky -c ~/.conky/conkyrc1 &

exit
