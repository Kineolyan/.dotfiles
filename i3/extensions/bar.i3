# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
        status_command i3status --config ~/.dotfiles/i3/bar-confs/i3status

	position top
}
