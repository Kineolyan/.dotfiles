# Mouse movements
# Configured to move mouse out of the way, if needed
# This is particularly cool for Intellij with linux
set $mode_mouse_move Mouse move directions
mode "$mode_mouse_move" {
    bindsym $left_letter exec --no-startup-id xdotool mousemove_relative -- -3000 0, mode "default"
    bindsym $right_letter exec --no-startup-id xdotool mousemove_relative -- 3000 0, mode "default"
    bindsym $up_letter exec --no-startup-id xdotool mousemove_relative -- 0 -3000, mode "default"
    bindsym $down_letter exec --no-startup-id xdotool mousemove_relative -- 0 3000, mode "default"

    # same bindings, but for the arrow keys
    bindsym Left exec --no-startup-id xdotool mousemove_relative -- -3000 0, mode "default"
    bindsym Right exec --no-startup-id xdotool mousemove_relative -- 3000 0, mode "default"
    bindsym Up exec --no-startup-id xdotool mousemove_relative -- 0 -3000, mode "default"
    bindsym Down exec --no-startup-id xdotool mousemove_relative -- 0 3000, mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+Shift+m mode "$mode_mouse_move"