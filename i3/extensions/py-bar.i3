# Start i3bar to display a workspace bar using py3status
# The configuration file must be defined in ~/.config/i3/i3status.conf
# or ~/.config/py3status/config
bar {
        status_command py3status -c ~/.dotfiles/i3/bar-confs/enabled

	position bottom
	separator_symbol "|"
}
