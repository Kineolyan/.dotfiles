# reload the configuration file
bindsym $mod+Shift+j reload

# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+p restart

# exit i3 (logs you out of your X session)
bindsym $mod+Shift+period exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# Power management
set $mode_system System (l) lock, (e) logout, (s) shutdown, (r) reboot, (h) suspend
mode "$mode_system" {
    bindsym l exec --no-startup-id i3lock -c 000000, mode "default"
    bindsym e exec --no-startup-id i3-msg exit, mode "default"
    bindsym s exec --no-startup-id systemctl poweroff, mode "default"
    bindsym r exec --no-startup-id systemctl reboot, mode "default"
    bindsym h exec --no-startup-id systemctl suspend, mode "default"

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+Shift+q mode "$mode_system"
