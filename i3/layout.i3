# change focus
bindsym $mod+$left_letter focus left
bindsym $mod+$up_letter focus down
bindsym $mod+$down_letter focus up
bindsym $mod+$right_letter focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+$left_letter move left
bindsym $mod+Shift+$up_letter move down
bindsym $mod+Shift+$down_letter move up
bindsym $mod+Shift+$right_letter move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

# split in horizontal orientation
bindsym $mod+d split h

# split in vertical orientation
bindsym $mod+k split v

# enter fullscreen mode for the focused container
bindsym $mod+u fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+o layout stacking
bindsym $mod+comma layout tabbed
bindsym $mod+period layout toggle split

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child