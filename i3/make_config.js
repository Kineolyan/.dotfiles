#!/usr/bin/env node
// @flow

const path = require('path');
const fs = require('fs');
const {spawn} = require('child_process');

const BASE = path.dirname(process.argv[1]);
const CONFIG_FILE = path.join(BASE, 'i3-config');
console.log('Configuration generated in', CONFIG_FILE);

const toPromise = (func) => (...args) => new Promise((resolve, reject) => {
		try {
			func(...args, (err, result) => {
				err ? reject(err) : resolve(result);
			});
		} catch (execErr) {
			reject(execErr);
		}
	});

const readFile/*: (string) => Promise<Buffer> */ = toPromise(fs.readFile);
const writeFile/*: (string, string) => Promise<void>*/ = toPromise(fs.writeFile);
const appendFile/*: (string, string) => Promise<void>*/ = toPromise(fs.appendFile);

/*:: type StartCommand = {
	command: string,
	workspace?: number
} */

/*:: type Configuration = {
	modifier: string,
	keyboard: string,
	wallpapers?: string,
	config: {
		terminal: string,
		browser: string,
		"file-explorer": string
	},
	extensions: Array<string>,
	starters?: Array<StartCommand>
} */
const configuration/*: Configuration*/ = require('./i3.json');
configuration['_paths_'] = {
	'i3-root': path.dirname(process.argv[1])
};

const configFile = path.join.bind(null, BASE);
const transferFile/* (string) => Promise<void> */ = fileName =>
	appendFile(CONFIG_FILE, `\n## config from ${fileName}\n`)
		.then(() => readFile(configFile(fileName)))
		.then(data => processFile(data, configuration))
		.then(data => appendFile(CONFIG_FILE, data.toString()))
		.then(() => appendFile(CONFIG_FILE, '\n'));

const objectValue = (root, path) => path.reduce(
	(obj, key) => {
		if (Reflect.has(obj, key)) {
			return obj[key];
		} else {
			throw new Error(`Key ${key} does not exist in ${JSON.stringify(obj)}`);
		}
	},
	root);
const processFile = (data, keys) => {
	const regexp = /\{\{([\w\-_.]+)\}\}/g;
	const input = data.toString('utf-8');
	let result = '';
	let i = 0;
	let match;
	while ((match = regexp.exec(input)) !== null) {
		const idx = match.index;
		const length = match[0].length;
		const varName  = match[1].split('.');
		const value = objectValue(keys, varName);

		result += input.substring(i, idx);
		result += value;
		i = idx + length;
	}

	if (i === 0) {
		return data;
	} else {
		// Add the remaining of the content
		if (i < input.length) {
			result += input.substring(i);
		}

		return result;
	}
};

const Vars = {
	MODIFIER: '$mod',
	LEFT_LETTER: '$left_letter',
	RIGHT_LETTER: '$right_letter',
	UP_LETTER: '$up_letter',
	DOWN_LETTER: '$down_letter'
};

// TODO extend configuration to bind every key to a var
function getMoveLetters(config/*: Configuration*/) {
	// It is best that move letters be located on the right side of the keyboard
	switch (config.keyboard) {
	case 'dvp': return {
			[Vars.LEFT_LETTER]: 'h',
			[Vars.RIGHT_LETTER]: 's',
			[Vars.UP_LETTER]: 't',
			[Vars.DOWN_LETTER]: 'n'
		};
	default: return {
			[Vars.LEFT_LETTER]: 'j',
			[Vars.RIGHT_LETTER]: ';',
			[Vars.UP_LETTER]: 't',
			[Vars.DOWN_LETTER]: 'l'
		}
	}
}

function getTopLetters(config/*: Configuration*/)/*: string[]*/ {
	switch (config.keyboard) {
	case 'dvp': return [
			'ampersand',
			'bracketleft',
			'braceleft',
			'braceright',
			'parenleft',
			'equal',
			'asterisk',
			'parenright',
			'plus',
			'bracketright'
		];
	default: return [
			'1',
			'2',
			'3',
			'4',
			'5',
			'6',
			'7',
			'8',
			'9',
			'0'
		]
	}
}

function generateMoveLetters(config/*: Configuration*/)/*: string*/ {
	const letters = getMoveLetters(config);
	return Object.keys(letters)
		.map(keyVar => `set ${keyVar} ${letters[keyVar]}`)
		.join('\n');
}

function generateTopLetters(config/*: Configuration*/)/*: string*/ {
	return getTopLetters(config)
		.map((key, i) => `set $top_${i + 1} ${key}`)
		.join('\n');
}

function getBasicConfig(config/*: Configuration*/)/*: string*/ {
	return `
## Standard configuration
# start a terminal
bindsym $mod+Return exec ${config.config.terminal}
`;
}

function getWallpaperConfig(config/*: Configuration*/)/*: string*/ {
	if (config.wallpapers) {
		const wallpaperBin = `${config['_paths_']['i3-root']}/tools/set-background.sh`;
		const action = `${wallpaperBin} ${config.wallpapers}`;
		return `
## Wallpapers
# Set it at startup
exec --no-startup-id ${action}
# Add a binding to switch it
bindsym $mod+Shift+g exec ${action}
`;
	} else {
		return '';
	}
}

function getStartCommands(config/*: Configuration */)/*: string*/ {
	if (config.starters !== undefined) {
		const basicCmds = config.starters
			.filter(st => st.workspace === undefined)
			.map(st => `exec ${st.command}`);
		const workspaceCmds = config.starters
			.filter(st => st.workspace !== undefined)
			.map(st => `exec --no-startup-id i3-msg 'workspace ${st.workspace}; exec ${st.command}; workspace 1'`);

		return `
## Basic commands for startup
${basicCmds.join('\n')}
## Start up in specific workspaces
${workspaceCmds.join('\n')}
`;
	} else {
		return '';
	}
}

function getAppShortcuts(config) {
	if (config.apps) {
		const bindings = Object.keys(config.apps)
			.map((key) => {
				if (key.length > 1) {
					throw new Error(`Shortcut for app must be of an char. Got: ${key}`);
				}

				return `bindsym ${key} exec ${config.apps[key].cmd}`;
			});
		const list = Object.keys(config.apps)
			.map((key) => `(${key}) ${config.apps[key].description || config.apps[key].cmd}`);
		const modeName = 'mode_shortcut';
		return `
## Shortcut mode for apps
set $${modeName} ${list.join(', ')}
mode "$${modeName}" {
${bindings.map(b => `\t${b}`).join('\n')}

	# back to normal: Enter or Escape
	bindsym Return mode "default"
	bindsym Escape mode "default"
}
bindsym $mod+Shift+f mode "$${modeName}"
`
	} else {
		return '';
	}
}

const checkConfiguration /*: () => Promise<boolean> */= () => new Promise((resolve, reject) => {
	try {
		const process = spawn('i3', ['-c', CONFIG_FILE, '-C']);
		let hasError = false;
		process.stdout.on('data', (out) => { hasError = true; });
		process.on('close', code => resolve(code === 0 && !hasError));
	} catch (execErr) {
		reject(execErr);
	}
});

writeFile(
	CONFIG_FILE,
`## Generated configuration file for i3
## Created by ${path.basename(process.argv[1])} at ${new Date().toLocaleString()}

set ${Vars.MODIFIER} ${configuration.modifier}
${generateMoveLetters(configuration)}
${generateTopLetters(configuration)}

`)
	.then(() => transferFile('ui.i3'))
	.then(() => appendFile(CONFIG_FILE, getWallpaperConfig(configuration)))
	.then(() => appendFile(CONFIG_FILE, getBasicConfig(configuration)))
	.then(() => transferFile('layout.i3'))
	// TODO for workspace,
	.then(() => transferFile('workspace.i3'))
	.then(() => transferFile('client.i3'))
	.then(() => configuration.extensions.reduce(
		(process, file) => process.then(() => transferFile(path.join('extensions', file))),
		Promise.resolve()
	))
	.then(() => appendFile(CONFIG_FILE, getStartCommands(configuration)))
	.then(() => appendFile(CONFIG_FILE, getAppShortcuts(configuration)))
	.then(() => checkConfiguration())
	.then(correct => {
		console.log(`Generation completed. ${correct ? 'Valid' : 'Invalid'} configuration generated`);
		process.exit(correct ? 0 : 1)
	});
