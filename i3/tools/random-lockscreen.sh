#!/bin/bash

# THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
DOT_DIR="$HOME/.dotfiles"
I3_HOME="$DOT_DIR/i3"

# Create the cache directory if not existing yet
CACHE_DIR="$I3_HOME/.cache"
mkdir -p "$CACHE_DIR"
LOCK_SCRIPT=$CACHE_DIR/lock.sh
if [ ! -f $LOCK_SCRIPT ]
then
  url='https://raw.githubusercontent.com/ShikherVerma/i3lock-multimonitor/master/lock'
  curl "$url" -o "$LOCK_SCRIPT"

  # Small patch to the file, before my PR is merged
  sed -i 's/mkdir \$CACHE_FOLDER/mkdir -p \$CACHE_FOLDER/' "$LOCK_SCRIPT"

  # Add a small hint about the origin of the file
  echo -e "\n# From: $url" >> "$LOCK_SCRIPT"

  chmod +x "$LOCK_SCRIPT"
fi

SCREEN_SAVER_DIR="$DOT_DIR/screensavers"
picture=$(ls $SCREEN_SAVER_DIR/* | sort -R | tail -n1)
if [ -n $picture ]
then
  $LOCK_SCRIPT -i "$picture"
else
  i3lock -c 000000
fi