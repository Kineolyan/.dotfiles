#!/bin/bash

WALLPAPER_DIR=$1
feh --randomize --bg-fill "$WALLPAPER_DIR"/*