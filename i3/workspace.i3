# switch to workspace
bindsym $mod+$top_1 workspace 1
bindsym $mod+$top_2 workspace 2
bindsym $mod+$top_3 workspace 3
bindsym $mod+$top_4 workspace 4
bindsym $mod+$top_5 workspace 5
bindsym $mod+$top_6 workspace 6
bindsym $mod+$top_7 workspace 7
bindsym $mod+$top_8 workspace 8
bindsym $mod+$top_9 workspace 9
bindsym $mod+$top_10 workspace 10

# move focused container to workspace
bindsym $mod+Shift+$top_1 move container to workspace 1
bindsym $mod+Shift+$top_2 move container to workspace 2
bindsym $mod+Shift+$top_3 move container to workspace 3
bindsym $mod+Shift+$top_4 move container to workspace 4
bindsym $mod+Shift+$top_5 move container to workspace 5
bindsym $mod+Shift+$top_6 move container to workspace 6
bindsym $mod+Shift+$top_7 move container to workspace 7
bindsym $mod+Shift+$top_8 move container to workspace 8
bindsym $mod+Shift+$top_9 move container to workspace 9
bindsym $mod+Shift+$top_10 move container to workspace 10

# Move workspace to screens
bindsym $mod+Ctrl+$up_letter move workspace to output up
bindsym $mod+Ctrl+$down_letter move workspace to output down
bindsym $mod+Ctrl+$left_letter move workspace to output left
bindsym $mod+Ctrl+$right_letter move workspace to output right

# same bindings, but for the arrow keys
bindsym $mod+Ctrl+Up move workspace to output up
bindsym $mod+Ctrl+Down move workspace to output down
bindsym $mod+Ctrl+Left move workspace to output left
bindsym $mod+Ctrl+Right move workspace to output right