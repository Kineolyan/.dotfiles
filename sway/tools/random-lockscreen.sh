#!/bin/bash

DOT_DIR="$HOME/.dotfiles"
SCREEN_SAVER_DIR="$DOT_DIR/screensavers"
picture=$(ls $SCREEN_SAVER_DIR/* | sort -R | tail -n1)
if [ -n $picture ]
then
  swaylock -c 000000 -s fit -i "$picture"
else
  swaylock -c 000000
fi