#!/bin/bash

WALLPAPER_DIR=$1
## FIXME this should be random
BG_FILE=$(ls -1 "$WALLPAPER_DIR" | head -n 1)
swaybg --mode fill --image "$WALLPAPER_DIR/$BG_FILE"
