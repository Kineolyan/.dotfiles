set number
set tabstop=2
set softtabstop=2
set cursorline
set lazyredraw
set showmatch

set incsearch           " search as characters are entered
set hlsearch            " highlight match
" turn off search highlight
nnoremap <leader><space> :nohlsearch<CR>

" Backup configuration
set backup
set backupdir=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set backupskip=/tmp/*,/private/tmp/*
set directory=~/.vim-tmp,~/.tmp,~/tmp,/var/tmp,/tmp
set writebackup

" Dvorak mapping
no d h
no h j
no t k
no n l
no s :
no S :
" Delete -> Junk
no j d
" Next -> Look next
no l n
" Previous -> Look back
no L N
" -- Dvorak additional features
no - $
no _ ^
no N <C-w><C-w>
no T <C-w><C-r>
no D <C-w><C-r>
