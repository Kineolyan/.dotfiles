With the latest linux, the dvp keyboard does not specify the l3 modifier.

It is possible to restore it, by hand, each time with the command `setxkbmap us dvp -option lv3:ralt_switch`.

To make it permanent, if existing, one can change or create a file in _/etc/X11/xorg.conf.d/_:

```
# 00-custom-kbd.conf
Section "InputClass"
        Identifier "system-keyboard"
        MatchIsKeyboard "on"
        Option "XkbLayout" "us,us"
        Option "XkbVariant" "dvp,"
        Option "XkbOptions" "lv3:ralt_switch,"
EndSection
```

In the above, the important line is **XkbOptions"**, adding the missing lv3 option.
