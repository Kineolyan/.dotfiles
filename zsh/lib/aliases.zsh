# Handy alias for sudo actions
# See https://askubuntu.com/a/22043 for details
alias sudo='sudo '

# enable color support of ls and also add handy aliases
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'

    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
    alias rgrep='rgrep --color=auto'
    alias Rgrep="grep -R --color=auto"
fi

# some more ls aliases
alias ll='ls -alFh'
alias la='ls -A'
alias l='ls -CF'
alias lla='ls -alh'

# Add an "alert" alias for long running commands.  Use like so:
#   sleep 10; alert
if type foo >/dev/null 2>&1
then
  alias alert='notify-send --urgency=low -i "$([ $? = 0 ] && echo terminal || echo error)" "$(history|tail -n1|sed -e '\''s/^\s*[0-9]\+\s*//;s/[;&|]\s*alert$//'\'')"'
fi

# Basic commands
alias less="less -R"
alias rm="rm -I --preserve-root"

# Admin alias
alias psudo="sudo env PATH='$PATH' "

# basic binaries
alias rvim='vim -R'

# Git
alias gka='gitk --all -n 5000'
alias gf='git fetch -p'

# Mount
alias cmount='mount | column -t'

# Network
# Stop after sending count ECHO_REQUEST packets #
alias ping='ping -c 5'
# Do not wait interval 1 second, go fast #
alias fastping='ping -c 100 -s.2'
alias connected='ping -c3 www.google.com'
alias ports='netstat -tulanp'

# Disk usage
alias df='df -H'
alias du='du -ch'

alias scopy='xclip -selection clipboard'

# Exa
alias exl='exa -l'
