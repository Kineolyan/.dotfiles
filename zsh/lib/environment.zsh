for _path in "$HOME/bin" "$HOME/node_modules/.bin" "$HOME/.rbenv/bin" "$HOME/.cargo/bin" "$HOME/.config/yarn/global/node_modules/.bin" "$HOME/.deno/bin" "$HOME/.local/bin"
do
	[ -d "$_path" ] && export PATH="$_path:$PATH"
done

if [ -d "$HOME/.rbenv/bin" ]
then # rbenv installed, initialize it
	 eval "$(rbenv init -)"
fi

export NVM_DIR="$HOME/.nvm"
if [ -s "$NVM_DIR/nvm.sh" ]
then # initialive nvm
	. "$NVM_DIR/nvm.sh"
fi

if [ -d "$HOME/.asdf" ]
then
	source "$HOME/.asdf/asdf.sh"
	source "$HOME/.asdf/completions/asdf.bash"
	alias aoeu="asdf "
fi

