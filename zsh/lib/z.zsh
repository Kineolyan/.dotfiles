#!/bin/bash

# THIS_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"
DOT_DIR="$HOME/.dotfiles"
ZSH_HOME="$DOT_DIR/zsh"

# Create the cache directory if not existing yet
CACHE_DIR="$ZSH_HOME/.cache"
mkdir -p "$CACHE_DIR"
Z_SCRIPT=$CACHE_DIR/z.sh
if [ ! -f $Z_SCRIPT ]
then
  url='https://raw.githubusercontent.com/rupa/z/9d5a3fe0407101e2443499e4b95bca33f7a9a9ca/z.sh'
  curl "$url" -o "$Z_SCRIPT"

  # Add a small hint about the origin of the file
  echo -e "\n# From: $url" >> "$Z_SCRIPT"
fi

source "$Z_SCRIPT"
