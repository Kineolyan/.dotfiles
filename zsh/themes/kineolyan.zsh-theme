prompt_time() {
  echo -ne " {%T} \e[7m%n\e[0m"
}

prompt_path() {
  echo -n " %{$fg[blue]%}[%~]"
}

prompt_git() {
  # Default from owner
  # ZSH_THEME_GIT_PROMPT_PREFIX="git:(%{$fg[red]%}"
  # ZSH_THEME_GIT_PROMPT_SUFFIX="%{$reset_color%}"
  # ZSH_THEME_GIT_PROMPT_DIRTY="%{$fg[blue]%}) %{$fg[yellow]%}✗%{$reset_color%}"
  # ZSH_THEME_GIT_PROMPT_CLEAN="%{$fg[blue]%})"

  # YS theme
  YS_VCS_PROMPT_PREFIX1="%{$fg[white]%}on%{$reset_color%} "
  YS_VCS_PROMPT_PREFIX2=":%{$fg[cyan]%}"
  YS_VCS_PROMPT_SUFFIX="%{$reset_color%}"
  YS_VCS_PROMPT_DIRTY=" %{$fg[red]%}x"
  YS_VCS_PROMPT_CLEAN=" %{$fg[green]%}o"

  # Git info.
  local git_info='$(git_prompt_info)'
  ZSH_THEME_GIT_PROMPT_PREFIX="${YS_VCS_PROMPT_PREFIX1}git${YS_VCS_PROMPT_PREFIX2}"
  ZSH_THEME_GIT_PROMPT_SUFFIX="$YS_VCS_PROMPT_SUFFIX"
  ZSH_THEME_GIT_PROMPT_DIRTY="$YS_VCS_PROMPT_DIRTY"
  ZSH_THEME_GIT_PROMPT_CLEAN="$YS_VCS_PROMPT_CLEAN"

  # Perso
  ZSH_THEME_GIT_PROMPT_EQUAL_REMOTE='='
  ZSH_THEME_GIT_PROMPT_AHEAD_REMOTE='+'
  ZSH_THEME_GIT_PROMPT_BEHIND_REMOTE='-'
  local remote_info="$(git_remote_status)"

  echo -n " %{$fg_bold[blue]%}$(git_prompt_info)"
  [ -n "$remote_info" ] && echo -n " ($remote_info)"
}

prompt_status() {
  echo -n "%(?:%{$fg_bold[green]%}➜ :%{$fg_bold[red]%}%? ➜ %s)"
}

prompt_tail() {
  echo -n "%{$reset_color%}"
}

prompt_jobs() {
  paused=$(jobs -s | wc -l)
  backgrounded=$(jobs -r | wc -l)
  if [[ $paused != 0 ]] || [[ $backgrounded != 0 ]]
  then
    echo -n "%{$fg_bold[green]%}[$backgrounded | $paused]"
  fi
}

## Main prompt
build_prompt() {
  prompt_time
  prompt_path
  prompt_jobs
  prompt_git
  echo -ne "\n "
  prompt_status
  prompt_tail
}
PROMPT='%{%f%b%k%}$(build_prompt) '
